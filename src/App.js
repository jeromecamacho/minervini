import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// custom style
import './css/bootstrap.min.css';
import './css/custom.css';
import './css/temp.css';
import './css/all.css';
import 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700';
import 'https://use.fontawesome.com/releases/v5.2.0/css/all.css';
import './DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.css';


// ***************************************

//                  NAVIGATION

// ***************************************
const Navigation = () => (
  <nav class="navbar navbar-expand navbar-dark topNav py-0">
          <div class="boxBorder">
              <a class="navbar-brand log" href="#">
                  <img src="img/logo-sm.png" />
              </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
              <ul class="navbar-nav">
                  <li class="nav-item ">
                      <div class="boxBorderR1">
                          <a class="nav-link text-center" href="#">
                              <img src="img/icons/email.png" />
                              <img src="img/icons/email-b.png" /> Email Us
                          </a>
                      </div>
                  </li>
              </ul>
          </div>
      </nav>
);

// ***************************************

//                  BODY

// ***************************************

class Body extends Component{

  render(){

    let headerInlineStyle = {
      backgroundColor: '#fff'
    };

    let badgeInlineStyle = {
      background:'url(img/login-bg.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center left'
    }

    return(
        <div>
          <section>
                <div class="row" style={headerInlineStyle}>
                    <div class="col-md-4 offset-md-1 col-sm-12 logSec">
                        <div class="loginTxt mb-4">
                            <h3>Welcome to</h3>
                            <h1 class="h3 mb-3 font-weight-normal">Minervini Private Access</h1>
                            <p>Enter your email address and password below</p>
                        </div>
                        <form class="form-signin">
                            <div class="form-label-group mb-3">
                                <label for="inputEmail">Email address</label>
                                <input type="email" id="inputEmail" class="form-control" placeholder="Enter email address" required="" autofocus="" autocomplete="off" />
                            </div>

                            <div class="form-label-group">
                                <label for="inputPassword">Password</label>
                                <input type="password" id="inputPassword" class="form-control mb-2" placeholder="Enter password" required="" autocomplete="off" />
                              
                            </div>
                            <div class="form-label-group">
                                            <label class="pointer">
                                                <input class="mb-5" type="checkbox" name="remember"/> Remember me
                                            </label>
                                        </div>
                            
                            <button class="btn btn-lg btn-primary btn-block gradBtn" type="submit">Login</button>
                            <p class="mt-1 mb-3 subP"><a href="forgot-password.html">Forgot your password?</a></p>
                            
                            <p class="mt-1 mb-3 subP"><a href="http://dev.fadworks.com/mike/minervini/sign-up/01-subscription.html">Dont have an account? click here to register</a></p>
                            
                        </form>
                    </div>

                    <div class="col-md-7  col-sm-12 d-none d-md-block" style={badgeInlineStyle}>
                        <div class="rightBg">
                            <img src="img/login-badge.png" />
                        </div>
                    </div>
                </div>
            </section>
            
          <section class="logInfo">
              <div class="container">
                  <div class="row">
                      <div class="col-12">
                          <p>This online delivery platform is a general publication dedicated to the education of investors and online stock traders; an information service only. The information provided herein is not to be construed as an offer to buy or sell stocks of any kind. The newsletter selections are not to be considered a recommendation to buy any stock but to aid the investor in making an informed decision based on technical and fundamental analysis. It is possible at this or some subsequent date, the editors and staff of Minervini Private Access, LLC (MPA) and Minervinipa.com may or may not own, buy or sell stocks presented. All investors should consult a qualified professional before trading any stock. The information provided has been obtained from sources deemed reliable but is not guaranteed as to accuracy or completeness. MPA staff and MinerviniPA.com make every effort to provide timely information to subscribers but cannot guarantee specific delivery times due to factors beyond our control. Mark Minervini is not an investment advisor, nor a securities broker. The information provided here is made to investors and speculators in general and without regard to your investment goals, financial circumstances, investment knowledge and your abilities or personal attitudes towards investing or trading abilities. Investors can and do lose money from investments. You agree to hold Mark Minervini and Minervini Private Access, LLC completely harmless as a result of reading this Web site or Newsletter. Contrary to what is written or communicated here in this publication, all references to "Buy" "Sell" "Buy Alert" "Sell Alert" "Sell Short" "Buy to Cover" or the like, including any comments, commentary or recommendations with regard to stocks or the financial markets should not be construed as a recommendation or advice that you in fact should buy the securities or take any particular course of action, as it is a mere form of expression on the writers part. The Information that will be provided in this website newsletter is not to be relied upon for your investment decision. Your decision to buy any securities is as a result your own free will and your own research. Minervini Private Access is not a model portfolio; THIS PUBLICATION IS FOR EDUCATIONAL PURPOSES ONLY! By subscribing to MPA and its related services, you agree that all material in the web site Minervini.com and/or any related web sites and the newsletter "Minervini Private Access" are for a single purchaser's eyes and e-mail address, text message address, etc. only. Should you decide to e-mail or to post this newsletter or any portion of this web site in whole or in part or any material elsewhere, your membership will be terminated and you will forfeit the remaining balance of your subscription, without refund. All data is for your personal use only; it may not be posted on web sites, message boards, communicated verbally, or be used in products or services for sale. All material in this website and its related websites and pages are protected under copyright laws of the United States. Unauthorized forwarding, copying or reproduction will be treated as a breach of copyright. In addition to the terms above, you agree to read and be bound by any additional terms of use posted in this web site and related web sites under the tab "Terms of Use". By logging into this web site and clicking the "Login" button, you acknowledge that you have read and agree to the terms outlined above. You also acknowledge that your subscription will automatically renew and your credit card will be charged unless you notify us at least 1-week in advance of your renewal date.</p>
                      </div>
                  </div>
              </div>
          </section>    
              
          <section class="logFoot">
              <div class="container">
                  <div class="row align-items-center">
                      <div class="col-md-6">
                          <a href="#"><img src="img/minervini-logo-light.png"/></a>
                      </div>
                      <div class="col-md-6 text-right footSocial">
                          <a href="#">Follow Mark On <img src="img/icons/twitter.png"/></a>
                      </div>
                  </div>
              </div>
          </section>
        </div>
    );
    }
};


// ***************************************

//                  FOOTER

// ***************************************
const Footer = () => (

  <footer class="mt-0">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <p> 2009-2018 Copyright © Minervini Private Access, LLC. All rights reserved. Do not duplicate or redistribute in any form.
                        <br/> SEPA® Specific Entry Point Analysis® Minervini Select® Leadership Profile® are registered trademarks.</p>
                    <p class="copyright">© 2018 Minervini Private Access, LLC</p>
                </div>
            </div>
        </div>
  </footer>

);


class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigation />
        <Body />
        <Footer />
      </div>
    );
  }
}

export default App;
